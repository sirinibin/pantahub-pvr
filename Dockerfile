FROM golang:alpine as builder

ENV GO111MODULE=on
RUN apk add -U git gcc

WORKDIR /go/src/gitlab.com/pantacor/pantahub-pvr
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

FROM alpine

COPY --from=builder /go/bin/pantahub-pvr /

ENTRYPOINT ["/pantahub-pvr"]

