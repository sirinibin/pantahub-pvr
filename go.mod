module gitlab.com/pantacor/pantahub-pvr

go 1.13

replace github.com/ant0ine/go-json-rest => github.com/asac/go-json-rest v3.3.3-0.20191004094541-40429adaafcb+incompatible

replace github.com/go-resty/resty => gopkg.in/resty.v1 v1.11.0

replace github.com/genuinetools/reg => github.com/jaswdr/reg v0.16.0-jaswdr

replace github.com/coreos/clair => github.com/jaswdr/clair v2.0.8-jaswdr+incompatible

replace github.com/fundapps/go-json-rest-middleware-jwt => github.com/pantacor/go-json-rest-middleware-jwt v0.0.0-20190329235955-213479ac018c

require (
	github.com/gorilla/mux v1.7.3
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/pantacor/pantahub-base v0.0.0-20190815210257-4b32d1858bed
	gitlab.com/pantacor/pvr v0.0.0-20191101134032-06011b587d9a
	gopkg.in/resty.v1 v1.12.0
)
