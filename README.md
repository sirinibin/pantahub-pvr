# Building

```
go get -v -u gitlab.com/pantacor/pantahub-pvr
go install gitlab.com/pantacor/pantahub-pvr
```

# running

```
PANTAHUB_API_URL=https://localhost:12365 pantahub-pvr
```

Default http port we listen on is 12367. TLS is not yet supported.

## Using

To get pvr by user-nick and device nick try:

```
pvr clone http://localhost:12367/user1/yourdevicenick
```

This will get you the latest posted revision.

To get a specific revision (in this example '2') use:

```
pvr clone http://localhost:12367/user1/yourdevicenick/2
```

# Resources

* Homepage: https://www.pantahub.com
* GIT: https://gitlab.com/pantacor/pantahub-pvr

